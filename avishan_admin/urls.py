from django.urls import path

from avishan_admin.views import *

urlpatterns = [
    path('', index),
    path('login', login),
    path('logout', logout),
    path('<str:model_plural_name>', model_list),
    path('<str:model_plural_name>/create', model_create),
    path('<str:model_plural_name>/<int:item_id>/detail', model_detail),
    path('<str:model_plural_name>/<int:item_id>/edit', model_update),
    path('<str:model_plural_name>/<int:item_id>/delete', model_delete),
]
