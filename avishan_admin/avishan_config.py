from avishan import current_request
from avishan_admin.utils import AvishanAdminBase
from avishan_admin_config import AvishanAdminConfig


def check():
    pass


def check_request():
    current_request['context'].update({
        'models': AvishanAdminBase.get_available_admin_model_pages(),
        'dashboard_fa_icon': AvishanAdminConfig.ADMIN_PANEL_FA_ICON,
        'dashboard_title': AvishanAdminConfig.ADMIN_PANEL_TITLE,
        'user_icon_image_address': "avishan_admin/img/user_icon.png",
        'PANEL_ROOT': AvishanAdminConfig.ADMIN_PANEL_ROOT_ADDRESS,
        'LOGIN_URL': AvishanAdminConfig.ADMIN_PANEL_LOGIN_ADDRESS,
        'ADMIN_PANEL_HEAD': AvishanAdminConfig.ADMIN_PANEL_HEAD
    })
    try:
        current_request['context']['user_full_name'] = current_request['base_user'].user.name
    except:
        pass
    try:
        current_request['context']['user_role'] = current_request['user_group'].title
    except:
        pass
