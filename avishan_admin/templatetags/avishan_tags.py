from django import template
from django.db import models

from avishan.models import AvishanModel

register = template.Library()


@register.filter
def field_class_name(field: models.Field):
    if field.__class__ == bool:
        return 'BooleanField'
    return field.__class__.__name__


@register.filter
def model_from_relational_field(field: models.ForeignKey):
    return field.related_model.__class__


@register.filter
def all_model_from_relational_field(field):
    return field.related_model.objects.all()


@register.filter
def in_list(value, the_list):
    value = str(value)
    return value in the_list.split(',')


@register.filter
def get_attribute(item: AvishanModel, attribute_name: str):
    return item.__getattribute__(attribute_name)


@register.filter
def translator(text: str):
    # todo check for all naming cases
    if isinstance(text, models.Field):
        return translator(text.name)
    text = str(text)
    try:
        from avishan_admin_config import AvishanAdminConfig
        return AvishanAdminConfig.ADMIN_DICTIONARY[text.lower()]['FA']
    except:
        pass

    dictionary = {
        'phone': {'FA': 'شماره همراه'},
        'password': {'FA': 'رمز عبور'},
        'enter': {'FA': 'ورود'},
        'user': {'FA': 'کاربر'},
        'title': {'FA': 'عنوان'},
        'year': {'FA': 'سال'},
        'month': {'FA': 'ماه'},
    }
    try:
        return dictionary[text.lower()]['FA']
    except KeyError:
        return text
