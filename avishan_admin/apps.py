from django.apps import AppConfig


class AvishanAdminConfig(AppConfig):
    name = 'avishan_admin'
