from typing import Union

from django.db import models
from django.shortcuts import render, redirect
from django.contrib import messages

from avishan import current_request
from avishan.decorators import AvishanTemplateView
from avishan.exceptions import ErrorMessageException
from avishan.misc import status, translatable
from avishan.models import AvishanModel, File, Image
from avishan_admin_config import AvishanAdminConfig
from avishan_admin.utils import AvishanAdminBase, AvishanAdminModel


# todo automatically handle messages
# todo logout

@AvishanTemplateView(authenticate=False)
def login(request):
    authentication_type = AvishanAdminConfig.ADMIN_PANEL_AUTHENTICATION_TYPE

    if request.method == 'POST':
        identifier = request.POST['identifier']
        password = request.POST['password']

        from core.models import User
        User.login(identifier, password)
        return redirect('/' + AvishanAdminConfig.ADMIN_PANEL_ROOT_ADDRESS)

    current_request['context']['authentication_type'] = authentication_type

    return render(request, 'pages/login.html', context=current_request['context'])


@AvishanTemplateView(authenticate=False)
def logout(request):
    if current_request['authentication_object']:
        current_request['authentication_object'].logout()
    else:
        current_request['add_token'] = False
    return redirect(
        '/' + AvishanAdminConfig.ADMIN_PANEL_ROOT_ADDRESS + "/" + AvishanAdminConfig.ADMIN_PANEL_LOGIN_ADDRESS,
        permanent=True)


@AvishanTemplateView()
def index(request):
    return redirect("/" + AvishanAdminConfig.ADMIN_PANEL_REDIRECT_INDEX, permanent=True)


@AvishanTemplateView()
def model_list(request, model_plural_name):
    model = AvishanAdminBase.get_admin_model_by_plural_name(model_plural_name)
    if model is None:
        raise ErrorMessageException('Model not found', status.HTTP_406_NOT_ACCEPTABLE)

    current_request['context']['model'] = model
    current_request['context']['items'] = model.objects.all().order_by('-id')
    current_request['context']['content_header_subtitle'] = ""
    return render(request, 'pages/model_list.html', context=current_request['context'])


@AvishanTemplateView()
def model_create(request, model_plural_name):
    model: Union[AvishanAdminModel, AvishanModel] = AvishanAdminBase.get_admin_model_by_plural_name(
        model_plural_name)
    if model is None:
        raise ErrorMessageException('Model not found', status.HTTP_406_NOT_ACCEPTABLE)

    if request.method == 'POST':
        added_files = {}
        for field in model.get_fields():
            if isinstance(field, models.ForeignKey) and field.related_model in [File, Image]:
                file = request.FILES.get(field.name, None)
                if file is not None:
                    created = field.related_model.objects.create()
                    created.file.save("files/" + file.name, file, save=True)
                    added_files[field.name] = created
                if model.is_field_required(field):
                    raise ErrorMessageException(
                        f'field {field.name} is required'
                    )
        created = model.create(**{**current_request['request'].data, **added_files})
        return redirect(f"/{AvishanAdminConfig.ADMIN_PANEL_ROOT_ADDRESS}/{model_plural_name}/{created.id}/detail",
                        permanent=True)

    current_request['context']['model'] = model
    current_request['context']['content_header_subtitle'] = "ایجاد"
    current_request['context']['submit_button_text'] = "ثبت"
    fields, file_added = model.admin_manage_data_fields(create_model=model)
    current_request['context']['fields'] = fields
    current_request['context']['file_added'] = file_added
    return render(request, 'pages/model_create.html', context=current_request['context'])


@AvishanTemplateView()
def model_detail(request, model_plural_name, item_id):
    model = AvishanAdminBase.get_admin_model_by_plural_name(model_plural_name)
    if model is None:
        raise ErrorMessageException('Model not found', status.HTTP_406_NOT_ACCEPTABLE)

    try:
        item = model.objects.get(id=item_id)
    except model.DoesNotExist:
        raise ErrorMessageException('Item with selected id not found')

    current_request['context']['item'] = item
    current_request['context']['model'] = model
    current_request['context']['content_header_subtitle'] = "جزئیات"
    current_request['context']['content_head'] = item.admin_item_head()
    current_request['context']['description_bodies'] = item.admin_get_description_bodies()

    return render(request, 'pages/model_detail.html', context=current_request['context'])


@AvishanTemplateView()
def model_update(request, model_plural_name, item_id):
    model = AvishanAdminBase.get_admin_model_by_plural_name(model_plural_name)
    if model is None or model.admin_prevent_edit():
        raise ErrorMessageException('Model not found', status.HTTP_406_NOT_ACCEPTABLE)

    try:
        item = model.objects.get(id=item_id)
    except model.DoesNotExist:
        raise ErrorMessageException('Item with selected id not found')

    if request.method == 'POST':
        added_files = {}
        for field in model.get_fields():
            if isinstance(field, models.ForeignKey) and field.related_model in [File, Image]:
                file = request.FILES.get(field.name, None)
                if file is not None:
                    created = field.related_model.objects.create()
                    created.file.save("files/" + file.name, file, save=True)
                    added_files[field.name] = created
                if model.is_field_required(field):
                    raise ErrorMessageException(
                        f'field {field.name} is required'
                    )
        updated_item = item.update(**{**current_request['request'].data, **added_files})
        return redirect(f"/{AvishanAdminConfig.ADMIN_PANEL_ROOT_ADDRESS}/{model_plural_name}/{updated_item.id}/detail",
                        permanent=True)

    current_request['context']['model'] = model
    current_request['context']['item'] = item
    current_request['context']['content_header_subtitle'] = "ویرایش"
    current_request['context']['submit_button_text'] = "ثبت"
    fields, file_added = item.admin_manage_data_fields(update_item=item)
    current_request['context']['fields'] = fields
    current_request['context']['file_added'] = file_added
    return render(request, 'pages/model_edit.html', context=current_request['context'])


@AvishanTemplateView()
def model_delete(request, model_plural_name, item_id):
    model = AvishanAdminBase.get_admin_model_by_plural_name(model_plural_name)
    if model is None:
        raise ErrorMessageException('Model not found', status.HTTP_406_NOT_ACCEPTABLE)

    try:
        model.objects.get(id=item_id).remove()
        messages.success(current_request['request'], translatable(
            EN=f'{model.class_name()} with id {item_id} deleted',
            FA=f'حذف شد',
        ))
    except model.DoesNotExist:
        raise ErrorMessageException('Item with selected id not found')
    return redirect(f"/{AvishanAdminConfig.ADMIN_PANEL_ROOT_ADDRESS}/{model_plural_name}", permanent=True)
