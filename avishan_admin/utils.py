from typing import Type, Optional, List, Union

from django.db import models
from django.db.models import NOT_PROVIDED

from avishan.models import AvishanModel, File, Image


class AvishanAdminBase:
    @staticmethod
    def get_available_admin_model_pages() -> List[Type['AvishanModel']]:
        return [item for item in AvishanAdminModel.__subclasses__() if item._admin_page]

    @staticmethod
    def get_admin_model_by_plural_name(model_plural_name: str) -> Optional['AvishanModel']:
        for model in AvishanAdminBase.get_available_admin_model_pages():
            if model.class_plural_snake_case_name() == model_plural_name:
                return model
        return None


class AvishanAdminModel:
    _admin_page = True
    _admin_name: str = None
    _admin_plural_name: str = None
    _admin_list_display = []
    _admin_filters = []
    _admin_fa_icon = None
    _admin_create_except_fields = []
    _admin_edit_except_fields = []
    _admin_item_title = ""
    _admin_item_subtitle = ""
    _admin_description_bodies = []
    _admin_related_model_names = []
    _translate_field_name = {}
    _admin_prevent_edit = False
    language = 'FA'

    # todo property bashe ya classmethod
    @property
    def admin_name(self):
        if self._admin_name is None:
            return self.class_name()
        return self._admin_name

    @property
    def admin_page(self):
        return self._admin_page

    @classmethod
    def admin_prevent_edit(cls):
        return cls._admin_prevent_edit

    @property
    def admin_plural_name(self):
        return self._admin_plural_name if self._admin_plural_name else self.admin_name + " ها"

    @property
    def admin_fa_icon(self):
        return self._admin_fa_icon

    @classmethod
    def admin_list_display(cls) -> List[models.Field]:
        if len(cls._admin_list_display) == 0:
            return [cls.get_field(item.name) for item in cls.get_full_fields()[1:]][:5]
        # todo except id. not slice
        return [cls.get_field(item.name) for item in cls._admin_list_display]

    def admin_list_display_data(self) -> List:
        all_data = []
        for field in self.admin_list_display():
            all_data.append(self.get_data_from_field(field.name))
        return all_data

    @classmethod
    def admin_filters(cls):
        if len(cls._admin_filters) == 0:
            return None
        return {
            'fields': [item for item in cls._admin_filters]
        }

    @staticmethod
    def admin_manage_data_fields(create_model: Optional[Union[AvishanModel, 'AvishanAdminModel']] = None,
                                 update_item: Optional[Union[AvishanModel, 'AvishanAdminModel']] = None):
        data = []
        target_model = create_model if create_model else update_item.__class__
        file_added = False

        for field in [item for item in target_model.get_full_fields()
                      if item not in target_model._admin_create_except_fields
                         and item.primary_key is False
                      ]:
            if target_model.is_field_readonly(field):
                continue
            temp, file = target_model.__handle_field_form_input(create_model, update_item, field)
            file_added = file or file_added
            data.append(temp)
        if create_model:
            data += target_model.admin_related_models_form_field_data(update_item)
        return data, file_added

    @classmethod
    def __relation_with_model(cls, related_model: Type[AvishanModel]):
        for field in related_model.get_fields():
            if isinstance(field, models.OneToOneField) and field.related_model == cls:
                return models.OneToOneField
            elif isinstance(field, models.ForeignKey) and field.related_model == cls:
                return models.ForeignKey
        return 'FUCK'

    @classmethod
    def translate_field_name(cls, field: models.Field) -> str:
        try:
            return cls._translate_field_name[field.name][cls.language]
        except KeyError:
            return 'NOT_TRANSLATED'

    @classmethod
    def admin_get_date_names(cls: AvishanModel):
        names = []
        for field in cls.get_full_fields():
            if isinstance(field, (models.DateField, models.DateTimeField)):
                names.append(field.name)
        return names

    def admin_item_title(self):
        title = {'size': 2}
        if isinstance(self._admin_item_title, list):
            output = ""
            for title_part in self._admin_item_title:
                if isinstance(title_part, models.Field):
                    output += str(self.get_data_from_field(title_part.name))
                else:
                    output += title_part
            title['data'] = output
        elif len(self._admin_item_title) > 0:
            title['data'] = self._admin_item_title
        else:
            title['data'] = self.get_data_from_field(self.get_fields()[1].name)
        return title

    def admin_item_subtitle(self):
        subtitle = {'size': 5}
        if isinstance(self._admin_item_subtitle, list):
            output = ""
            for title_part in self._admin_item_subtitle:
                if isinstance(title_part, models.Field):
                    output += str(self.get_data_from_field(title_part.name))
                else:
                    output += title_part
            subtitle['data'] = output
        else:
            subtitle['data'] = self._admin_item_subtitle
        return subtitle

    def admin_item_head(self):
        return {
            'title': self.admin_item_title(),
            'subtitle': self.admin_item_subtitle(),
            'type': 'straight'
        }

    def admin_get_description_bodies(self):
        bodies = []
        if len(self._admin_description_bodies) == 0:
            body = {
                'title': {
                    'size': 3,
                    'data': "",
                },
                'type': 'straight',
                'parts': []
            }
            for field in self.get_fields():
                body['parts'].append({
                    'data': self.get_data_from_field(field.name),
                    'field': field,
                    'size': 5
                })
            bodies.append(body)
        else:
            for description_body in self._admin_description_bodies:
                body = {
                    'title': {
                        'size': 3,
                        'data': description_body['title']
                    },
                    'type': 'straight',
                    'parts': []
                }
                for field in description_body['fields']:
                    body['parts'].append({
                        'data': self.get_data_from_field(field.name),
                        'field': field,
                        'size': 6
                    })
                bodies.append(body)
        for related_model in self.admin_related_models():
            show_flag = False
            body = {
                'title': {
                    'size': 3,
                    'data': related_model.class_snake_case_name(),
                },
                'type': 'related',
                'description_bodies': []

            }
            for related_model_field in related_model.get_fields():
                if isinstance(related_model_field,
                              models.ForeignKey) and related_model_field.related_model == self.__class__:
                    target_field_on_related_model = related_model_field
                    break
            for related_object in related_model.filter(**{related_model_field.name: self}):
                show_flag = True
                temp = related_object.admin_get_description_bodies()
                for temp_part in temp:
                    target_index = None
                    for i, temp_part_part in enumerate(temp_part['parts']):
                        if temp_part_part['field'] == target_field_on_related_model:
                            target_index = i
                            break
                    if target_index is not None:
                        temp_part['parts'].pop(target_index)

                    target_index = None
                    for i, temp_part_part in enumerate(temp_part['parts']):
                        if temp_part_part['field'].name == 'id':
                            target_index = i
                            break
                    if target_index is not None:
                        temp_part['parts'].pop(target_index)

                body['description_bodies'].append(temp)

            if show_flag:
                bodies.append(body)
        return bodies

    @classmethod
    def admin_related_models(cls) -> List[Type[AvishanModel]]:
        return [cls.get_model_with_class_name(item) for item in cls._admin_related_model_names]

    @classmethod
    def admin_related_models_form_field_data(cls, update_item=None):
        data = []
        # todo update item
        for related_model in cls.admin_related_models():
            data.append(cls.add_related_model_to_fields(cls, related_model))
        return data

    @staticmethod
    def __handle_field_form_input(create_model, update_item, field):
        from avishan_admin.templatetags.avishan_tags import translator

        target_model = create_model if create_model else update_item.__class__
        field_data = {
            'field': field,
            'label': translator(field.name),
            'required': target_model.is_field_required(field)
        }
        file_added = False

        if len(field.choices) > 0:
            target_model.__handle_choice_select_form_input(create_model, field, field_data, update_item)
        elif isinstance(field, (models.OneToOneField, models.ForeignKey)) and field.related_model not in [File,
                                                                                                          Image]:
            target_model.__handle_related_field_form_input(create_model, field, field_data, update_item)
        elif isinstance(field, models.ManyToManyField):
            target_model.__handle_many_to_many_field_form_input(create_model, field, field_data,
                                                                update_item)
        elif isinstance(field, models.DateTimeField):
            field_data['type'] = 'datetime'
            pass
        elif isinstance(field, models.DateField):
            field_data['type'] = 'date'
            pass
        elif isinstance(field, models.TimeField):
            field_data['type'] = 'time'
            pass
        elif isinstance(field, models.TextField):
            target_model.__handle_text_field_form_input(create_model, field, field_data, update_item)
        elif isinstance(field, (models.IntegerField, models.AutoField, models.FloatField)):
            target_model.__handle_number_field_form_input(create_model, field, field_data, update_item)
        else:
            target_model.__handle_else_field_form_input(create_model, field, field_data, update_item)
            if isinstance(field, models.ForeignKey) and field.related_model is File:
                file_added = True
                field_data['type'] = 'file'

        return field_data, file_added

    @staticmethod
    def __handle_choice_select_form_input(create_model, field, field_data, update_item):
        target_model = create_model if create_model else update_item.__class__
        field_data['type'] = 'choices'
        field_data['options'] = []
        if not field_data['required']:
            field_data['options'].append({
                'value': '0',
                'label': '---',
                'selected': True if (
                    (field.default or field.null or field.blank)
                    if create_model
                    else update_item.get_data_from_field(
                        field.name) is None)
                else False,
                'disabled': False
            })
        else:
            field_data['options'].append({
                'value': '0',
                'label': 'انتخاب کنید',
                'selected': True,
                'disabled': True
            })
        for choice in field.choices:
            field_data['options'].append({
                'value': str(choice[0]),
                'label': choice[1],
                'selected': True if (field.default if create_model else update_item.get_data_from_field(
                    field.name)) is choice[0] else False
            })

    @staticmethod
    def __handle_else_field_form_input(create_model, field, field_data, update_item):
        field_data['type'] = 'text'
        field_data['data'] = "" if (field.default is NOT_PROVIDED and create_model) else (
                field.default and create_model or update_item.get_data_from_field(field.name))
        if field_data['data'] is None:
            field_data['data'] = ""
        else:
            field_data['data'] = str(field_data['data'])

    @staticmethod
    def __handle_number_field_form_input(create_model, field, field_data, update_item):
        field_data['type'] = 'number'
        if isinstance(field, models.FloatField):
            field_data['step'] = 'any'
        else:
            field_data['step'] = '1'
        field_data['data'] = ""
        if create_model:
            if field.default is not NOT_PROVIDED:
                field_data['data'] = field.default
        else:
            field_data['data'] = update_item.get_data_from_field(field.name)
            if field_data['data'] is None:
                field_data['data'] = ""

    @staticmethod
    def __handle_text_field_form_input(create_model, field, field_data, update_item):
        field_data['type'] = 'textarea'
        field_data['data'] = "" if (create_model and field.default is NOT_PROVIDED) else (
            field.default if create_model else update_item.get_data_from_field(field.name))
        if field_data['data'] is None:
            field_data['data'] = ""
        else:
            field_data['data'] = str(field_data['data'])

    @staticmethod
    def __handle_many_to_many_field_form_input(create_model, field, field_data, update_item):
        field_data['type'] = 'models'
        field_data['options'] = []
        selected = [] if create_model else update_item.get_data_from_field(field.name)
        for related_model_item in field.related_model.objects.all().order_by('-id'):
            field_data['options'].append({
                'value': related_model_item.id,
                'label': str(related_model_item),
                'selected': True if related_model_item in selected else False,
                'disabled': False
            })

    @staticmethod
    def __handle_related_field_form_input(create_model, field, field_data, update_item):
        field_data['type'] = 'model'
        field_data['options'] = []
        if not field_data['required']:
            field_data['options'].append({
                'value': '0',
                'label': '---',
                'selected': True if (
                    (field.default or field.null or field.blank)
                    if create_model
                    else update_item.get_data_from_field(
                        field.name) is None)
                else False,
                'disabled': False
            })
        else:
            field_data['options'].append({
                'value': '0',
                'label': 'انتخاب کنید',
                'selected': True,
                'disabled': True
            })
        if isinstance(field, models.ForeignKey):
            related_model_items = field.related_model.objects.all()
        else:
            related_model_items = field.related_model.objects.filter()
        for related_model_item in field.related_model.objects.all():
            field_data['options'].append({
                'value': related_model_item.id,
                'label': str(related_model_item),
                'selected': True if (update_item and update_item.get_data_from_field(field.name) is not None
                                     and update_item.get_data_from_field(
                            field.name).id == related_model_item.id) else False,
                'disabled': False
            })

    @classmethod
    def add_related_model_to_fields(cls, base_model: Union[AvishanModel, 'AvishanAdminModel'],
                                    target_model: Union[AvishanModel, 'AvishanAdminModel']) -> dict:

        data = {
            'type': 'adder',
            'label': target_model._admin_name,
            'inner_fields': target_model.admin_manage_data_fields(target_model)[0],
            'field': {
                'name': target_model.class_snake_case_name()
            }
        }
        for i, field_data in enumerate(data['inner_fields']):
            if isinstance(field_data['field'], models.ForeignKey) and field_data['field'].related_model == base_model:
                data['inner_fields'].pop(i)
                break
        return data
